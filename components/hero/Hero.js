import Image from 'next/image'
import logo from '../../public/assets/images/hero/manual-symbol.svg'
import heroStyles from './Hero.module.scss'



function Hero({ setQuizIsStarted }) {

  const startQuiz = () => {
    setQuizIsStarted(true);
  };

  return (
    <div className={heroStyles.background}>
      <nav>
        <Image src={logo} alt='' width='50' height='50' />
      </nav>
      <div className={heroStyles.container}>
        <div className={heroStyles.lines}>
          <h1>Be good to yourself</h1>
          <p> We’re working around the clock to bring you a holistic approach to your wellness. From top to bottom, inside and out. </p>
        </div>

        <button className={heroStyles.btn} onClick={startQuiz}> TAKE THE QUIZ </button>
      </div>

    </div >
  )
}

export default Hero;
