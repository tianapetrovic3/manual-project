import stl from './Questions.module.scss'

function QuestionImg({ value }) {

  return (
    <>
      <div className={stl.image} dangerouslySetInnerHTML={{ __html: value }} />

    </>
  );
}

export default QuestionImg;
