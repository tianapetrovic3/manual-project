import stl from './Questions.module.scss'

function QuestionTxt({ value }) {

  return (
    <>
      <div className={stl.text} dangerouslySetInnerHTML={{ __html: value }} />
    </>
  );
}

export default QuestionTxt;
