import { useState } from 'react'
import QuestionTxt from './QuestionTxt'
import QuestionImg from './QuestionImg'

function Questions({
  index,
  setIndex,
  type,
  value,
  setQuizState,
  questionsLength,
  isRejection,
  setQuizResult
}) {

  const initialIndex = () => {
    if (index < questionsLength) {
      setIndex(index + 1);
    }
  };

  const chooseResult = () => {
    initialIndex();

    if (isRejection) {
      setQuizResult('no');
      setQuizState('completed');
    }

    if (index + 1 === questionsLength && !isRejection) {
      setQuizResult('yes');
      setQuizState('completed');
    }
  };

  return (
    <div onClick={chooseResult}>
      {
        type === 'img'
          ? (<QuestionImg value={value} />)
          : (<QuestionTxt value={value} />)
      }
    </div>
  );
}

export default Questions;
