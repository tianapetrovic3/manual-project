import { useState, useEffect } from 'react'
import Questions from './Questions'
import stl from './Questions.module.scss'


function QuestionsOverview({
  index,
  setIndex,
  quizContent,
  setQuizState,
  setQuizResult
}) {

  const [questionsArray, setquestionsArray] = useState(quizContent.questions);

  useEffect(() => {
    const displayQuestion = () => {

      let id = 0;
      let option;

      for (const [index, value] of questionsArray.entries()) {
        id += 1;
        value.id = id;

        for (const x in value.options) {
          option = value.options[x].value;
          value.questionOption = option;
        }

        if (value.options[0].display.includes('img')) {

          value.optionType = 'img';

        } else if (value.options[0].display.includes('No') || value.options[0].display.includes('Yes')
        ) {

          value.optionType = 'text';

        }
      }
    };
    displayQuestion();
  });

  return (
    <>
      <h1 className={stl.question}>
        {questionsArray[index].question}
      </h1>

      <div className={stl.option}>
        {
          questionsArray[index].options && questionsArray[index].options
            .map((opt) => {

              const idx = Object.keys(opt)[0];
              const value = opt[idx];

              return (
                <>
                  <Questions
                    key={idx}
                    index={index}
                    setIndex={setIndex}

                    value={value}
                    type={questionsArray[index].optionType}
                    questionsLength={questionsArray.length}

                    isRejection={opt.isRejection}
                    setQuizResult={setQuizResult}
                    setQuizState={setQuizState}
                  />
                </>
              );
            })
        }
      </div>
    </>
  );
}

export default QuestionsOverview;
