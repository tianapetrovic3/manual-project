import Link from 'next/link'
import stl from './Result.module.scss'


function Result({ quizResult }) {

  if (quizResult === 'no') {
    return (
      <div className={stl.container}>
        <p>
          Unfortunately, we are unable to prescribe this medication for you. This is because finasteride can alter the PSA levels, which maybe used to monitor for cancer.
          You should discuss this further with your GP or specialist if you would still like this medication.
        </p>
      </div>
    )
  } else if (quizResult === 'yes') {
    return (
      <div className={stl.container}>
        <p>
          Great news! We have the perfect treatment for your hair loss.
          Proceed to <Link href='https://www.manual.co' target='_blank' rel='noopener noreferrer'> www.manual.co </Link> and prepare to say hello to your new hair!
        </p>
      </div>
    )
  }

}

export default Result;
