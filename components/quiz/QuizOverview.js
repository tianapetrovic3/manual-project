import { useState, useEffect } from 'react'
import QuizContainer from './QuizContainer'
import QuestionsOverview from './questions/QuestionsOverview'
import Result from './result/Result'
import stl from './Quiz.module.scss'


function QuizOverview({
  displayQuiz,
  closeQuiz,
  quizContent,
  selectedOption,
  setSelectedOption,
  quizState,
  setQuizState,
  index,
  setIndex,
}) {

  const [arrLength, setArrLength] = useState();
  const [quizResult, setQuizResult] = useState();

  useEffect(() => {
    const length = quizContent.questions.length;
    setArrLength(length - 1);
  });

  const goBack = () => {
    if (index >= 0) {
      setIndex(index - 1);
      setQuizResult(null);
    }
    if (quizResult === 'no') {
      setQuizResult(null);
      setQuizState('inProgress');
    }
  };

  return (
    <>
      <QuizContainer displayQuiz={displayQuiz} onHide={closeQuiz} />

      <div className={stl.quizOverview} style={{ transform: displayQuiz ? 'translateY(0)' : 'translateY(-100vh)' }}>

        <section className={stl.contentContainer}>
          <button className={stl.btnClose} onClick={closeQuiz}> x </button>

          <div className={stl.quizContent}>
            {quizState === 'initial' || quizState === 'inProgress'
              ? (
                <QuestionsOverview
                  index={index}
                  setIndex={setIndex}
                  quizContent={quizContent}
                  setQuizState={setQuizState}
                  select={selectedOption}
                  setSelectedOption={setSelectedOption}
                  setQuizResult={setQuizResult}
                />
              )
              : (
                <Result quizResult={quizResult} />
              )}
          </div>

          <div style={{ paddingTop: 30 + 'px' }}>
            {index === 0 || quizResult === 'yes' ? (
              <div />
            ) : (
              <button className={stl.btn} onClick={() => goBack()}>Previous</button>
            )}
          </div>
        </section>
      </div>
    </>
  );
}

export default QuizOverview;
