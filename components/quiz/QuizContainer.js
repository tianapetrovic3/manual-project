import stl from './Quiz.module.scss'

const QuizContainer = (props) =>
  props.displayQuiz ?
    (<div className={stl.quizContainer} onClick={props.clicked}></div>)
    : null;

export default QuizContainer;
