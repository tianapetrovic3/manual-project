import Image from 'next/image'
import photo1 from '../../public/assets/images/landing/Photo-1.png'
import photo2 from '../../public/assets/images/landing/Photo-2.png'
import stl from './Landing.module.scss'


const Landing = () => {
    return (
        <div>
            <div className={stl.header}>
                <h2> What we can help with </h2>
            </div>

            <section className={stl.section}>
                <div className={stl.row}>
                    <div className={stl.img1Container}>
                        <Image src={photo1} alt='Hair' className={stl.img1} />
                    </div>
                    <div className={stl.box1}>
                        <h3>01</h3>
                        <h4>Hair loss</h4>
                        <h2>Hair loss needn’t be irreversible. We can help! </h2>
                        <p>We’re working around the clock to bring you a holistic approach to your wellness. From top to bottom, inside and out.</p>
                    </div>

                </div>
            </section>

            <section className={stl.section}>
                <div className={stl.row}>
                    <div className={stl.box2}>
                        <h3>02</h3>
                        <h4>Erecetile dysfunction</h4>
                        <h2>Erections can be a tricky thing. But no need to feel down!</h2>
                        <p>We’re working around the clock to bring you a holistic approach to your wellness. From top to bottom, inside and out.</p>
                    </div>
                    <div className={stl.img2Container}>
                        <Image src={photo2} alt='ED' className={stl.img2} />
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Landing
