import Image from 'next/image'
import Link from 'next/link'
import logo from '../../public/assets/images/hero/manual-symbol.svg'
import fbIcon from '../../public/assets/images/footer/facebook.png'
import twIcon from '../../public/assets/images/footer/twitter.png'
import gIcon from '../../public/assets/images/footer/google.png'
import stl from './Footer.module.scss'

const Footer = () => {
  return (
    <footer className={stl.footer}>
      <div className={stl.container}>
        <div style={{ padding: 30 + 'px' }}>
          <Image src={logo} alt='' width='70' height='70' />
        </div>
        <div className={stl.row}>
          <div className={stl.footercol}>
            <h4>
              Product
            </h4>
            <ul>
              <li><Link href='/'>Popular</Link></li>
              <li><Link href='/'>Trending</Link></li>
              <li><Link href='/'>Guided</Link></li>
              <li><Link href='/'>Products</Link></li>

            </ul>
          </div>

          <div className={stl.footercol}>
            <h4>
              Company
            </h4>
            <ul>
              <li><Link href='/'>Press</Link></li>
              <li><Link href='/'>Mission</Link></li>
              <li><Link href='/'>Strategy</Link></li>
              <li><Link href='/'>About</Link></li>

            </ul>
          </div>

          <div className={stl.footercol}>
            <h4>
              Info
            </h4>
            <ul>
              <li><Link href='/'>Support</Link></li>
              <li><Link href='/'>Customer Service</Link></li>
              <li><Link href='/'>Get started</Link></li>

            </ul>
          </div>

          <div className={stl.footercol}>
            <h4>
              Follow us
            </h4>
            <div className={stl.socials}>
              <ul>
                <li> <Image src={fbIcon} alt='' width='25' height='25' /></li>
                <li> <Image src={gIcon} alt='' width='25' height='25' /></li>
                <li> <Image src={twIcon} alt='' width='25' height='25' /></li>
              </ul>
            </div>
          </div>
        </div >
      </div >

      <div className={stl.divider}></div>

      <div className={stl.copy}>
        <p>© 2021 Manual. All rights reserverd</p>
      </div>
    </footer >
  )
}

export default Footer
