import { useState, useEffect } from 'react'
import Head from 'next/head'
import Hero from '../components/hero/Hero'
import Landing from '../components/landing/Landing'
import Footer from '../components/footer/Footer';
import QuizOverview from '../components/quiz/QuizOverview'

export const getStaticProps = async () => {
  const res = await fetch(`https://manual-case-study.herokuapp.com/questionnaires/972423.json`);

  const data = await res.json();

  return {
    props: { quizContent: data },
  };
};

export default function Home({ quizContent }) {
  const [quizIsStarted, setQuizIsStarted] = useState(false);
  const [quizState, setQuizState] = useState('initial');
  const [arrLength, setArrLength] = useState();
  const [index, setIndex] = useState(0);

  const closeQuiz = () => {
    setQuizIsStarted(false);
    setQuizState('initial');
    setIndex(0);
  };

  useEffect(() => {
    const length = quizContent.questions.length;
    setArrLength(length - 1);
  });

  return (
    <div>
      <Head>
        <title>Manual</title>
        <meta name='description' content='Only the best for your health' />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <QuizOverview
        displayQuiz={quizIsStarted}
        quizContent={quizContent}
        quizState={quizState}
        setQuizState={setQuizState}
        index={index}
        setIndex={setIndex}
        closeQuiz={closeQuiz}
      />

      <Hero setQuizIsStarted={setQuizIsStarted} />
      <Landing />
      <Footer />
    </div>
  );
}
